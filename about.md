---
layout: page
title: About
permalink: /about/
---

We are a group of students at Olin simulating remote work to build a hardware/software project.

We only communicate through the internet, add shipping delays, and have each have different roles and tools available.
