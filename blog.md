---
title: Blog
permalink: "/blog/"
layout: page
---


{% for post in site.posts %}
  <p class="post-meta">
  {% assign date_format = "%b %-d, %Y" %}
    <b style="font-size: 20px"><a class="post-link" href="{{ post.url | relative_url }}">{{ post.title | escape }}</a></b>
    &nbsp;&nbsp;({{ post.date | date: date_format }})
  </p>
{% endfor %}

<p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | relative_url }}">via RSS</a></p>
